FROM openjdk:13-jdk-alpine
RUN mkdir -p /usr/src/ontouml
WORKDIR /
COPY ./target/kprime-expert-ontouml-1.0-SNAPSHOT-jar-with-dependencies.jar ontouml.jar
CMD ["java", "-jar", "ontouml.jar"]