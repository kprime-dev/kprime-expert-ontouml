package it.unibz.cs.semint.kprime.expert

import it.unibz.cs.semint.kprime.expert.adapter.ExpertHttp
import org.junit.Test
import kotlin.test.assertEquals

class OntoServerTransformIT {

    @Test
    fun test_transform_class() {
        // given
        val sourceToTransform = """
            {
                "options": {
                    "baseIri": "http://myontology.com"
                },
                "project": {
                    "id" : "Pz4r2.6GAqACHQBO",
                    "type" : "Project",
                    "model" : {
                        "id" : "Pz4r2.6GAqACHQBO_root",
                        "type" : "Package",
                        "contents" : [ {
                            "id" : "mNir2.6GAqACHQWX",
                            "name" : "Person",
                            "type" : "Class",
                            "stereotype" : "kind",
                            "restrictedTo" : [ "functional-complex" ]
                        } ]
                    }
                }
            }
        """.trimIndent()
        val expertURL = "http://api.ontouml.org/v1/transform/gufo"
        // when
        val (_,postResponse) = ExpertHttp().post(expertURL, sourceToTransform)
        // then
        assertEquals("""
            {
              "result": "@prefix : <http://myontology.com#>.\n@prefix gufo: <http://purl.org/nemo/gufo#>.\n@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.\n@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.\n@prefix owl: <http://www.w3.org/2002/07/owl#>.\n@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.\n\n<http://myontology.com> rdf:type owl:Ontology;\n    owl:imports gufo:.\n:Person rdf:type owl:Class, gufo:Kind, owl:NamedIndividual;\n    rdfs:subClassOf gufo:FunctionalComplex;\n    rdfs:label \"Person\"@en.\n",
              "issues": []
            }
        """.trimIndent(),postResponse)

        """
        @prefix : <http://myontology.com#>.\n
|       @prefix gufo: <http://purl.org/nemo/gufo#>.
|       @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
|       @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
|       @prefix owl: <http://www.w3.org/2002/07/owl#>.
|       @prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
|       
|       <http://myontology.com> rdf:type owl:Ontology;
|           owl:imports gufo:.
|           :Person rdf:type owl:Class, gufo:Kind, owl:NamedIndividual;
            rdfs:subClassOf gufo:FunctionalComplex;
|           rdfs:label \"Person\"@en.
|       """".trimMargin()
    }
}