package it.unibz.cs.semint.kprime.expert

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.json.JsonMapper
import it.unibz.cs.semint.kprime.expert.ontouml.schema.*
import org.junit.Test
import kotlin.test.assertEquals

class OntoumlProtocolTest {
    @Test
    fun test_root_package() {
        // given
        val root = OUMLRootPackage("package","pack1",null,null, emptyList())
        // when
        val json = JsonMapper()
                .enable(SerializationFeature.INDENT_OUTPUT)
                .writeValueAsString(root)
        // then
        assertEquals("""
            {
              "type" : "package",
              "id" : "pack1",
              "name" : null,
              "description" : null,
              "contents" : [ ]
            }
        """.trimIndent(),json)
    }


    @Test
    fun test_class() {
        // given
        val oumlClass = OUMLClass("class1","Class",null,null, null,null,null,null,null,null)
        val root = OUMLRootPackage("package","pack1",null,null, listOf(oumlClass))
        // when
        val json = JsonMapper()
                .enable(SerializationFeature.INDENT_OUTPUT)
                .writeValueAsString(root)
        // then
        assertEquals("""
            {
              "type" : "package",
              "id" : "pack1",
              "name" : null,
              "description" : null,
              "contents" : [ {
                "id" : "class1",
                "type" : "Class",
                "name" : null,
                "description" : null,
                "properties" : null,
                "literals" : null,
                "propertyAssignments" : null,
                "stereotypes" : null,
                "isAbstract" : null,
                "isDerived" : null
              } ]
            }
        """.trimIndent(),json)
    }

    @Test
    fun test_class_with_one_propoerty() {
        // given
        val oumlPropertyType = OUMLPropertyType("Class","class1")
        val oumlProperty = OUMLProperty("Property",id="prop1",name=null,null,null,null,null,oumlPropertyType,null,null,"",null,false,false)
        val oumlClass = OUMLClass("class1","Class",null,null, listOf(oumlProperty),null,null,null,null,null)
        val root = OUMLRootPackage("package","pack1",null,null, listOf(oumlClass))
        // when
        val json = JsonMapper()
                .enable(SerializationFeature.INDENT_OUTPUT)
                .writeValueAsString(root)
        // then
        assertEquals("""
            {
              "type" : "package",
              "id" : "pack1",
              "name" : null,
              "description" : null,
              "contents" : [ {
                "id" : "class1",
                "type" : "Class",
                "name" : null,
                "description" : null,
                "properties" : [ {
                  "type" : "Property",
                  "id" : "prop1",
                  "name" : null,
                  "description" : null,
                  "cardinality" : null,
                  "stereotypes" : null,
                  "propertyAssignments" : null,
                  "propertyType" : {
                    "type" : "Class",
                    "id" : "class1"
                  },
                  "subsettedProperties" : null,
                  "refinedProperties" : null,
                  "aggregationKind" : "",
                  "isReadOnly" : null,
                  "isDerived" : false,
                  "isOrdered" : false
                } ],
                "literals" : null,
                "propertyAssignments" : null,
                "stereotypes" : null,
                "isAbstract" : null,
                "isDerived" : null
              } ]
            }
        """.trimIndent(),json)
    }

    @Test
    fun test_request() {
        // given
        val options = mapOf("baseIri" to "http://myontology.com")
        val oumlPropertyType = OUMLPropertyType("Class","class1")
        val oumlProperty = OUMLProperty("Property",id="prop1",name=null,null,null,null,null,oumlPropertyType,null,null,"",null,false,false)
        val oumlClass = OUMLClass("class1","Class",null,null, listOf(oumlProperty),null,null,null,null,null)
        val root = OUMLRootPackage("package","pack1",null,null, listOf(oumlClass))

        val project = OUMLProject("Project","Pz4r2.6GAqACHQBO",root)
        val request = OUMLRequest(options,project)
        // when
        val json = JsonMapper()
                .enable(SerializationFeature.INDENT_OUTPUT)
                .writeValueAsString(request)
        // then
        assertEquals("""
            {
              "options" : {
                "baseIri" : "http://myontology.com"
              },
              "project" : {
                "type" : "Project",
                "id" : "Pz4r2.6GAqACHQBO",
                "model" : {
                  "type" : "package",
                  "id" : "pack1",
                  "name" : null,
                  "description" : null,
                  "contents" : [ {
                    "id" : "class1",
                    "type" : "Class",
                    "name" : null,
                    "description" : null,
                    "properties" : [ {
                      "type" : "Property",
                      "id" : "prop1",
                      "name" : null,
                      "description" : null,
                      "cardinality" : null,
                      "stereotypes" : null,
                      "propertyAssignments" : null,
                      "propertyType" : {
                        "type" : "Class",
                        "id" : "class1"
                      },
                      "subsettedProperties" : null,
                      "refinedProperties" : null,
                      "aggregationKind" : "",
                      "isReadOnly" : null,
                      "isDerived" : false,
                      "isOrdered" : false
                    } ],
                    "literals" : null,
                    "propertyAssignments" : null,
                    "stereotypes" : null,
                    "isAbstract" : null,
                    "isDerived" : null
                  } ]
                }
              }
            }
            """.trimIndent(),json)
    }
}