package it.unibz.cs.semint.kprime.expert

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.json.JsonMapper
import it.unibz.cs.semint.kprime.expert.adapter.ExpertHttp
import it.unibz.cs.semint.kprime.expert.ontouml.schema.*
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class OntoServerVerifyIT {

    @Test
    fun test_verify_ontouml_project() {
        // given
        val requestText = """
            {
                "options": {
                    "baseIri": "http://myontology.com"
                },
                "project": {
                    "id" : "Pz4r2.6GAqACHQBO",
                    "type" : "Project",
                    "model" : {
                        "id" : "Pz4r2.6GAqACHQBO_root",
                        "type" : "Package",
                        "contents" : [ {
                            "id" : "mNir2.6GAqACHQWX",
                            "name" : "Person",
                            "type" : "Class",
                            "stereotype" : "kind",
                            "restrictedTo" : [ "functional-complex" ]
                        } ]
                    }
                }
            }        
        """.trimIndent()
        val expertUrl = "http://api.ontouml.org/v1/verify"
        // when
        val postResult = ExpertHttp().post(expertUrl, requestText)
        val (_,postResponse) = postResult
        // then
        assertEquals("""
            {
              "result": []
            }
        """.trimIndent(),postResponse)
    }

    @Test
    fun test_verify_wrong_ontouml_project() {
        // given
        val requestText = """
            {
                "options": {
                    "baseIri": "http://myontology.com"
                },
                "project": {
                    "id" : "Pz4r2.6GAqACHQBO",
                    "type" : "Project",
                    "model" : {
                        "id" : "Pz4r2.6GAqACHQBO_root",
                        "type" : "Package",
                        "contents" : [ {
                            "id" : "mNir2.6GAqACHQWX",
                            "name" : "Person",
                            "type" : "Class",
                            "stereotype" : "kind",
                            "restrictedTo" : [ "functi-complex" ]
                        } ]
                    }
                }
            }        
        """.trimIndent()
        val expertUrl = "http://api.ontouml.org/v1/verify"
        // when
        val postResult = ExpertHttp().post(expertUrl, requestText)
        val (_,postResponse) = postResult
        // then
        assertEquals("""
            {
              "result": [
                {
                  "code": "class_incompatible_natures",
                  "title": "Incompatible stereotype and 'restrictedTo' combination",
                  "description": "The «kind» class Person has its value for 'restrictedTo' incompatible with the stereotype",
                  "severity": "error",
                  "data": {
                    "source": {
                      "stereotype": "kind",
                      "restrictedTo": [
                        "functi-complex"
                      ],
                      "properties": null,
                      "literals": null,
                      "isAbstract": false,
                      "isDerived": false,
                      "isExtensional": false,
                      "isPowertype": false,
                      "order": "1",
                      "propertyAssignments": {},
                      "type": "Class",
                      "id": "mNir2.6GAqACHQWX",
                      "name": "Person",
                      "description": null
                    },
                    "context": []
                  }
                }
              ]
            }
        """.trimIndent(),postResponse)
    }


    @Test
    fun test_verify_ontouml_project_protocol() {
        // given
        val options = mapOf("baseIri" to "http://myontology.com")
        val oumlPropertyType = OUMLPropertyType("Class","class1")
        val oumlProperty = OUMLProperty("Property",id="prop1",name=null,null,null,null,null,oumlPropertyType,null,null,"",null,
            isDerived = false,
            isOrdered = false
        )
        val oumlClass = OUMLClass("class1","Class",null,null, listOf(oumlProperty),null,null,null,null,null)
        val root = OUMLRootPackage("Package","pack1",null,null, listOf(oumlClass))

        val project = OUMLProject("Project","Pz4r2.6GAqACHQBO",root)
        val request = OUMLRequest(options,project)
        // when
        val jsonRequest = JsonMapper()
                .enable(SerializationFeature.INDENT_OUTPUT)
                .writeValueAsString(request)
        val expertUrl = "http://api.ontouml.org/v1/verify"
        // when
        val postResult = ExpertHttp().post(expertUrl, jsonRequest)
        val (_,postResponse) = postResult
        // then
        // skip id field and
        assertTrue(postResponse.endsWith("""
                  "status": 400,
                  "message": "The input could not be parse into a valid instance of Project.",
                  "info": [
                    {
                      "keyword": "type",
                      "dataPath": "/model",
                      "schemaPath": "#/properties/model/oneOf/0/type",
                      "params": {
                        "type": "null"
                      },
                      "message": "should be null"
                    },
                    {
                      "keyword": "type",
                      "dataPath": "/model/contents",
                      "schemaPath": "#/properties/contents/oneOf/0/type",
                      "params": {
                        "type": "null"
                      },
                      "message": "should be null"
                    },
                    {
                      "keyword": "const",
                      "dataPath": "/model/contents/0/type",
                      "schemaPath": "#/properties/type/const",
                      "params": {
                        "allowedValue": "Package"
                      },
                      "message": "should be equal to constant"
                    },
                    {
                      "keyword": "enum",
                      "dataPath": "/model/contents/0/properties/0/aggregationKind",
                      "schemaPath": "#/properties/aggregationKind/oneOf/0/enum",
                      "params": {
                        "allowedValues": [
                          "NONE",
                          "SHARED",
                          "COMPOSITE"
                        ]
                      },
                      "message": "should be equal to one of the allowed values"
                    },
                    {
                      "keyword": "type",
                      "dataPath": "/model/contents/0/properties/0/aggregationKind",
                      "schemaPath": "#/properties/aggregationKind/oneOf/1/type",
                      "params": {
                        "type": "null"
                      },
                      "message": "should be null"
                    },
                    {
                      "keyword": "oneOf",
                      "dataPath": "/model/contents/0/properties/0/aggregationKind",
                      "schemaPath": "#/properties/aggregationKind/oneOf",
                      "params": {
                        "passingSchemas": null
                      },
                      "message": "should match exactly one schema in oneOf"
                    },
                    {
                      "keyword": "type",
                      "dataPath": "/model/contents/0/properties",
                      "schemaPath": "#/oneOf/1/type",
                      "params": {
                        "type": "null"
                      },
                      "message": "should be null"
                    },
                    {
                      "keyword": "oneOf",
                      "dataPath": "/model/contents/0/properties",
                      "schemaPath": "#/oneOf",
                      "params": {
                        "passingSchemas": null
                      },
                      "message": "should match exactly one schema in oneOf"
                    },
                    {
                      "keyword": "const",
                      "dataPath": "/model/contents/0/type",
                      "schemaPath": "#/properties/type/const",
                      "params": {
                        "allowedValue": "Relation"
                      },
                      "message": "should be equal to constant"
                    },
                    {
                      "keyword": "const",
                      "dataPath": "/model/contents/0/type",
                      "schemaPath": "#/properties/type/const",
                      "params": {
                        "allowedValue": "GeneralizationSet"
                      },
                      "message": "should be equal to constant"
                    },
                    {
                      "keyword": "required",
                      "dataPath": "/model/contents/0",
                      "schemaPath": "#/required",
                      "params": {
                        "missingProperty": "general"
                      },
                      "message": "should have required property 'general'"
                    },
                    {
                      "keyword": "oneOf",
                      "dataPath": "/model/contents/0",
                      "schemaPath": "#/properties/contents/oneOf/1/items/oneOf",
                      "params": {
                        "passingSchemas": null
                      },
                      "message": "should match exactly one schema in oneOf"
                    },
                    {
                      "keyword": "oneOf",
                      "dataPath": "/model/contents",
                      "schemaPath": "#/properties/contents/oneOf",
                      "params": {
                        "passingSchemas": null
                      },
                      "message": "should match exactly one schema in oneOf"
                    },
                    {
                      "keyword": "oneOf",
                      "dataPath": "/model",
                      "schemaPath": "#/properties/model/oneOf",
                      "params": {
                        "passingSchemas": null
                      },
                      "message": "should match exactly one schema in oneOf"
                    }
                  ]
                }
        """.trimIndent()))
    }


}