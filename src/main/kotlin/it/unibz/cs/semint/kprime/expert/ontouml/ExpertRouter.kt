package it.unibz.cs.semint.kprime.expert.ontouml

import io.javalin.Javalin
import io.javalin.http.Handler
import it.unibz.cs.semint.kprime.expert.adapter.*

object ExpertRouter {

    const val EXPERT_NAME = "OntoUML"
    const val EXPERT_VERSION = "0.1"

    fun routesConfig(app: Javalin) {
        app.get("/", rootHandler)
//        app.get("/validate", ExpertController.validateTermsHandler)
//        app.get("/owl", ExpertController.transformDatabaseHandler)
        app.post("/verify", ExpertController.verifyHandler)
    }

    val rootHandler = Handler { ctx ->
        val result = """
        Hello from KPrime Expert $EXPERT_NAME $EXPERT_VERSION, available commands:
            verify - checks OntoUML ontological model validation. WIP returns a fixed response.
            transform - transformation of an OntoUML model into a gUFO-based OWL ontology. TODO.
    """.trimIndent()
        ctx.json(
            ExpertResponse(
                ExpertMessage("0", NoteLabel.ok, NoteLevel.info, result),
                null,
                ExpertOptions(listOf(
                    ExpertOption("Validate","validate"),
                    ExpertOption("OWL","owl")
                )
                )
            )
        )
    }
    
}