package it.unibz.cs.semint.kprime.expert.ontouml

import it.unibz.cs.semint.kprime.expert.adapter.ExpertHttp
import java.io.StringWriter

object ExpertService {

    fun transform(readDatabaseString: String, patternDir: String, params: Map<String, String>): Pair<StringWriter, String> {
        TODO()
    }

    fun verify(inputBody: String):String {
        val requestText = inputBody.trimIndent()
        val expertUrl = "http://api.ontouml.org/v1/verify"
        // when
        val postResult = ExpertHttp().post(expertUrl, requestText)
        val (_,postResponse) = postResult
        // then
        return postResponse
    }
}