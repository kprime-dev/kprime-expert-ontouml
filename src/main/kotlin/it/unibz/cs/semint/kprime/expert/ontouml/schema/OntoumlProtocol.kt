package it.unibz.cs.semint.kprime.expert.ontouml.schema

import com.fasterxml.jackson.annotation.JsonProperty
import liquibase.hub.model.Project
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlElementWrapper


sealed class OUMLContent()

data class OUMLRequest(
        val options: Map<String,String>,
        val project: OUMLProject
)

data class OUMLProject(
        val type: String,
        val id: String,
        val model: OUMLContent
)

typealias OUMLRootPackage = OUMLPackage

data class OUMLPackage(
        val type:String,
        val id:String,
        val name:String?,
        val description:String?,
        val contents:List<OUMLContent>?
): OUMLContent()

data class OUMLClass(
        val id:String,
        val type:String,
        val name:String?,
        val description:String?,
        val properties:List<OUMLProperty>?,
        val literals:List<String>?,
        val propertyAssignments:List<String>?,
        val stereotypes:List<String>?,
        private var isAbstract:Boolean?,
        private var isDerived:Boolean?
): OUMLContent() {
    @JsonProperty("isAbstract")
    @Override
    fun getIsAbstract(): Boolean? {
        return isAbstract
    }
    @Override
    fun setIsAbstract(isAbstract: Boolean?) {
        this.isAbstract = isAbstract
    }
    @JsonProperty("isDerived")
    @Override
    fun getIsDerived(): Boolean? {
        return isDerived
    }
    @Override
    fun setIsDerived(isDerived: Boolean?) {
        this.isDerived = isDerived
    }
}

data class OUMLProperty(
        val type:String = "object",
        val id:String,
        val name:String?,
        val description:String?,
        val cardinality:String?,
        val stereotypes:List<String>?,
        val propertyAssignments:List<String>?,
        val propertyType:OUMLPropertyType?,
        val subsettedProperties:List<String>?,
        val refinedProperties:List<String>?,
        val aggregationKind:String,
        private var isReadOnly:Boolean?,
        private var isDerived:Boolean?,
        private var isOrdered:Boolean?
) {
    @JsonProperty("isDerived")
    @Override
    fun getIsDerived(): Boolean? {
        return isDerived
    }
    @Override
    fun setIsDerived(isDerived: Boolean?) {
        this.isDerived = isDerived
    }

    @JsonProperty("isOrdered")
    @Override
    fun getIsOrdered(): Boolean? {
        return isOrdered
    }
    @Override
    fun setIsOrdered(isOrdered: Boolean?) {
        this.isOrdered = isOrdered
    }

    @JsonProperty("isReadOnly")
    @Override
    fun getIsReadOnly(): Boolean? {
        return isReadOnly
    }
    @Override
    fun setIsReadOnly(isReadOnly: Boolean?) {
        this.isReadOnly = isReadOnly
    }
    
}

data class OUMLPropertyType(val type:String, val id:String)

data class OUMLRelation(val id:String): OUMLContent()
data class OUMLGeneralization(val id:String): OUMLContent()
data class OUMLGeneralizationSet(val id:String): OUMLContent()

