package it.unibz.cs.semint.kprime.expert.ontouml

import io.javalin.Javalin

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
fun main() {
    val webAppPort = 7199
    val xslHome = System.getenv("KPRIME_XSL_HOME")
    val path = if (xslHome != null) xslHome+"vd_simple_ab/"
                        else ExpertController::class.java.getResource("/vd_simple_ab/").path
    println(" kprime_xsl_home = $path")

    val javalin = Javalin.create()
    javalin.config.showJavalinBanner = false
    ExpertRouter.routesConfig(javalin.start(webAppPort))
}