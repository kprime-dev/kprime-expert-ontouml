package it.unibz.cs.semint.kprime.expert.ontouml

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.javalin.http.Handler
import it.unibz.cs.semint.kprime.expert.adapter.*
import it.unibz.cs.semint.kprime.expert.domain.Term
import it.unibz.cs.semint.kprime.expert.domain.Todo
import it.unibz.cs.semint.kprime.expert.ontouml.ExpertRouter.EXPERT_NAME
import it.unibz.cs.semint.kprime.expert.ontouml.ExpertRouter.EXPERT_VERSION
import unibz.cs.semint.kprime.domain.db.Database
import unibz.cs.semint.kprime.domain.ddl.ChangeSet
import java.util.*
import kotlin.random.Random


object ExpertController {

    private const val PROJECT_ARG = "_prj"
    private const val TRACE_ARG = "_tr"
    private const val TRACE_FILE_ARG = "_tb"

    val http = ExpertHttp()
    
    val validateTermsHandler = Handler { ctx ->
        val termbase = ctx.queryParam(TRACE_FILE_ARG) ?: ""
        val project = ctx.queryParam(PROJECT_ARG) ?: ""
        val trace = ctx.queryParam(TRACE_ARG) ?: ""
        if (termbase.isEmpty() || project.isEmpty() || trace.isEmpty()) {
            ctx.json(
                ExpertResponse(
                    ExpertMessage("0", NoteLabel.not_found, NoteLevel.critical,
                            "Required argument termbase [$termbase] or project [$project] or trace [$trace] is empty."),
                    ExpertPayload(project,trace,termbase,"")
                )
            )
        } else {
            // get info from kp server
            val expertUrl = "${ctx.req.scheme}://${ctx.req.remoteAddr}:7000/expert/$project/dictionary/$trace/$termbase"
            println(expertUrl)
            val (_, response) = http.request(expertUrl)

            // put result payload to kp server
            val termsToPut = listOf(
                    Term("name1","categ1","rela","type1","http://schema.org","this is a $EXPERT_NAME term", "$EXPERT_NAME-$EXPERT_VERSION"),
                    Term("name2","categ1","rela","type2","http://schema.org","this is a $EXPERT_NAME term", "$EXPERT_NAME-$EXPERT_VERSION"),
            )
            http.put(expertUrl, ExpertPayload(project,trace,termbase,termsToPut))

            // respond to call
            val readValue = jacksonObjectMapper().readValue<List<Term>>(response)
            ctx.json(ExpertResponse(
                    ExpertMessage("0", NoteLabel.ok, NoteLevel.info,""),
                    ExpertPayload(project, trace, termbase, readValue))
            )
        }
    }

    val changeDatabaseHandler = Handler { ctx ->
        val termbase = ctx.queryParam(TRACE_FILE_ARG) ?: ""
        val project = ctx.queryParam(PROJECT_ARG) ?: ""
        val trace = ctx.queryParam(TRACE_ARG) ?: ""
        if (termbase.isEmpty() || project.isEmpty() || trace.isEmpty()) {
            ctx.json(ExpertResponse(
                    ExpertMessage("0", NoteLabel.not_found, NoteLevel.critical,
                            "Required argument termbase [$termbase] or project [$project] or trace [$trace] is empty."),
                    ExpertPayload(project,trace,termbase,""))
            )
        } else {
            // get info from kp server
            val expertUrlGet = "${ctx.req.scheme}://${ctx.req.remoteAddr}:7000/expert/$project/database/$trace/$termbase/json"
            println(expertUrlGet)
            val (_, response) = http.request(expertUrlGet)
            println(response)

            // put result payload to kp server
            val expertUrlPut = "${ctx.req.scheme}://${ctx.req.remoteAddr}:7000/expert/$project/changeset/$trace/$termbase"
            val changesetToPut = ChangeSet()
            changesetToPut.commands?.add("add-goal goal$EXPERT_NAME-$EXPERT_VERSION")
            val changesetString = jacksonObjectMapper().writeValueAsString(changesetToPut)
            http.put(expertUrlPut, ExpertPayload(project,trace,termbase,changesetString))

            // respond to call
            //val readValue = XMLSerializerJacksonAdapter().deserializeDatabase(response)
            val readValue = jacksonObjectMapper().readValue<Database>(response)
            ctx.json(ExpertResponse(
                    ExpertMessage("0", NoteLabel.ok, NoteLevel.info,""),
                    ExpertPayload(project, trace, termbase, readValue))
            )
        }
    }

    val transformDatabaseHandler = Handler { ctx ->
        val termbase = ctx.queryParam(TRACE_FILE_ARG) ?: ""
        val project = ctx.queryParam(PROJECT_ARG) ?: ""
        val trace = ctx.queryParam(TRACE_ARG) ?: ""

        println("-------------------------REQUEST:")
        println(ctx.path())
        println("------------------------------")

        if (termbase.isEmpty() || project.isEmpty() || trace.isEmpty()) {
            ctx.json(ExpertResponse(
                    ExpertMessage("0", NoteLabel.not_found, NoteLevel.critical,
                            "Required argument termbase [$termbase] or project [$project] or trace [$trace] is empty."),
                    ExpertPayload(project,trace,termbase,"")))
        } else {
            // get params from kp server
            val expertUrlParamsGet = "${ctx.req.scheme}://${ctx.req.remoteAddr}:7000/expert/$project/document/$trace/$termbase.md"
            val (statusDocReq, doc) = http.request(expertUrlParamsGet)
            println(statusDocReq)
            //if (statusDocReq == )
            val params = DocParser().parseAnswers(doc)

            // get info from kp server
            val expertUrlGet = "${ctx.req.scheme}://${ctx.req.remoteAddr}:7000/expert/$project/database/$trace/$termbase/xml"
            println(expertUrlGet)
            val (_, readDatabaseString) = http.request(expertUrlGet)
            println(readDatabaseString)

            val patternDir = when(ctx.path().substringAfterLast("/")) {
                "vsplit" -> "vd_simple_ab"
                "vjoin" -> "vd_simple_ba"
                else -> ""
            }
            if (patternDir.isEmpty()) {
                ctx.json(ExpertResponse(
                        ExpertMessage("0", NoteLabel.not_found, NoteLevel.critical,
                                "Required pattern dir but is empty."),
                        ExpertPayload(project,trace,termbase,"")))
            }

            val (outWriter, stepsDescription) = ExpertService.transform(readDatabaseString,patternDir,params)

            // put result payload to kp server
            val expertUrlPutDB = "${ctx.req.scheme}://${ctx.req.remoteAddr}:7000/expert/$project/database/$trace/$termbase/xml"
            val expertUrlPutMB = "${ctx.req.scheme}://${ctx.req.remoteAddr}:7000/expert/$project/document/$trace/$termbase.md"
            val databaseToPut = Database()
            http.put(expertUrlPutMB, ExpertPayload(project, trace, termbase, stepsDescription))
            //val databaseString = jacksonObjectMapper().writeValueAsString(databaseToPut)
            val databaseXML = outWriter.toString()
            if (databaseXML.isEmpty()) {
                // respond to call
                ctx.json(ExpertResponse(
                        ExpertMessage("0", NoteLabel.ok, NoteLevel.info, "Empty Database Result, see generated document."),
                        ExpertPayload(project, trace, termbase, databaseToPut))
                )
            } else {
                println("----------------OUT:")
                println(databaseXML)
                http.put(expertUrlPutDB, ExpertPayload(project, trace, termbase, databaseXML))

                // respond to call
                ctx.json(ExpertResponse(
                        ExpertMessage("0", NoteLabel.ok, NoteLevel.info, ""),
                        ExpertPayload(project, trace, termbase, databaseToPut))
                )
            }
        }
    }


    val validateTodoHandler = Handler { ctx ->
        val project = ctx.queryParam(PROJECT_ARG) ?: ""
        if (project.isEmpty()) {
            ctx.json(ExpertResponse(
                    ExpertMessage("0", NoteLabel.not_found, NoteLevel.critical,"For goals validation project argument is required."),
                    ExpertPayload(project,"","",""))
            )
        } else {
            // get info from pk server
            val expertUrl = "${ctx.req.scheme}://${ctx.req.remoteAddr}:7000/expert/$project/todo"
            println(expertUrl)
            // put indo to pk server
            val date = Date()
            val todoToPut = listOf(
                    Todo(Random.nextLong(),"title1",
                        completed = false,
                        hidden = false,
                        key = "root",
                        dateCreated = date,
                        dateOpened = date,
                        dateClosed = date,
                        dateDue = date,
                        priority = 0,
                        estimate = 1,
                        partof = "",
                        assignee = "",
                        isOpened = true,
                        isClosed = false,
                        labels = "$EXPERT_NAME-$EXPERT_VERSION"
                    ),
                    Todo(Random.nextLong(),"title2",
                        completed = false,
                        hidden = false,
                        key = "root",
                        dateCreated = date,
                        dateOpened = date,
                        dateClosed = date,
                        dateDue = date,
                        priority = 0,
                        estimate = 1,
                        partof = "",
                        assignee = "",
                        isOpened = true,
                        isClosed = false,
                        labels = "$EXPERT_NAME-$EXPERT_VERSION"
                    )
            )
            http.put(expertUrl, ExpertPayload(project,"","",todoToPut))


            // respond to call
            val (_, response) = http.request(expertUrl)
            val readValue = jacksonObjectMapper().readValue<List<Todo>>(response)
            ctx.json(ExpertResponse(
                    ExpertMessage("0", NoteLabel.ok, NoteLevel.info,"Created ${todoToPut.size} activities to do."),
                    ExpertPayload(project, "", "", readValue))
            )
        }
    }

    val verifyHandler = Handler { ctx ->
        val termbase = ctx.queryParam(TRACE_FILE_ARG) ?: ""
        val project = ctx.queryParam(PROJECT_ARG) ?: ""
        val trace = ctx.queryParam(TRACE_ARG) ?: ""
        val inputBody = ctx.body().replace(";",System.lineSeparator())

        println("-------------------------REQUEST verify:")
        println(ctx.path())
        println("POSTED: $inputBody")
        println("------------------------------")

        // respond to call
        val verifyResult = ExpertService.verify(inputBody)
        println("-------------------------RESPONSE verify:")
        println(verifyResult)
        println("------------------------------")
        ctx.json(ExpertResponse(
                ExpertMessage("0", NoteLabel.ok, NoteLevel.info,verifyResult),
                ExpertPayload(project, trace, termbase, verifyResult))
        )

    }

}