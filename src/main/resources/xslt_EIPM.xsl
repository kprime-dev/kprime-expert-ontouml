<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<!-- XML Input Files -->
<xsl:variable name="dp2" select="document('dbpattern_2nd.xml')/*" />
<!--
<xsl:variable name="dbex" select="document('dbinstance_ex.xml')/*" />
-->
<xsl:variable name="dbex" select="./*" />

<xsl:template match="/">
    <database>
        <schema>
            <tables>
                <!-- Matching tables from the expended database instance with the second transformation pattern -->
                <xsl:call-template name="table">
                	<xsl:with-param name="tpos" select="1"/>
                	<xsl:with-param name="dpattpos" select="1"/>
                	<xsl:with-param name="tname" select="$dp2//table[position() = 1]/@name"/>
                </xsl:call-template>
            </tables>
            <constraints>
                <!-- Matching constraints from the expended database instance with the second transformation pattern -->
                <xsl:call-template name="constraint">
                	<xsl:with-param name="cpos" select="1"/>
                	<xsl:with-param name="csattpos" select="1"/>
                	<xsl:with-param name="csname" select="$dp2//constraint[position() = 1]/source/@table"/>
                	<xsl:with-param name="ctattpos" select="1"/>
                	<xsl:with-param name="ctname" select="$dp2//constraint[position() = 1]/target/@table"/>
                </xsl:call-template>

            </constraints>
        </schema>
    </database>
</xsl:template>


<!-- The idea is to browse each attribute from each tables of the database pattern, starting with with first table, first attribute. In order to figure out the name of new table, we start by inputing the relation name from the pattern (usually a R and some numbers). Then, if there is a 'var' match between an attribute from the pattern and one from the instance, we concatenate the name of the instance table to the current table name (if we tables R and Employee matches, the new name of the table will be R-Employee. Then if R and Department match, the new name will be R-Employee-Department, and so on). -->
<xsl:template name="table">
	<xsl:param name="tpos"/>
	<xsl:param name="dpattpos"/>
	<xsl:param name="tname"/>
		<xsl:variable name="ctable" select="$dp2//table[position() = $tpos]"/>
		<xsl:choose>
			<!-- As long as the current attribute of indice $dpattpos is <= than the total number of attributes in a table of indice $tpos, we search attributes with the same 'var' within the expended database instance -->
			<xsl:when test="$dpattpos &lt;= count($ctable/columns/column)">
				<xsl:call-template name="tablesearch">
					<xsl:with-param name="tpos" select="$tpos"/>
		            <xsl:with-param name="dpattpos" select="$dpattpos"/>
		            <xsl:with-param name="ttablepos" select="1"/>
		            <xsl:with-param name="tname" select="$tname"/>
		            <xsl:with-param name="ctable" select="$ctable"/>
				</xsl:call-template>
			</xsl:when>
			<!-- Otherwise, we generate a table with an updated name corresponding to a relation in the second pattern. Then we match attriutes from the instance and the pattern together -->
			<xsl:otherwise>
				<table var="{$ctable/@name}" id="{$ctable/@id}" name="{$tname}">
					<columns>
						<xsl:call-template name="attcolumn">
							<xsl:with-param name="tpos" select="$tpos"/>
							<xsl:with-param name="ctable" select="$ctable"/>
							<xsl:with-param name="initattind" select="1"/>
							<xsl:with-param name="finalattind" select="1"/>
							<xsl:with-param name="targetind" select="1"/>
							<xsl:with-param name="targettablepos" select="1"/>
							<xsl:with-param name="atttype" select="'table'"/>
						</xsl:call-template>
					</columns>
				</table>
				<!-- Once we created a table, we test if it is the last one and if not we increment the table indice -->
				<xsl:if test="$tpos &lt; count($dp2//tables/table)">
					<xsl:call-template name="table">
	                	<xsl:with-param name="tpos" select="$tpos + 1"/>
	                	<xsl:with-param name="dpattpos" select="1"/>
	                	<xsl:with-param name="tname" select="$dp2//table[position() = $tpos +1]/@name"/>
	                </xsl:call-template>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
</xsl:template>

<!-- Parcouring the tables of the database instance in order of their relative position  -->
<xsl:template name="tablesearch">
	<xsl:param name="tpos"/>
	<xsl:param name="dpattpos"/>
	<xsl:param name="ttablepos"/>
	<xsl:param name="tname"/>
	<xsl:param name="ctable"/>
		 <xsl:variable name="targettable" select="$dbex//tables/table[position() = $ttablepos]"/>
		 <xsl:call-template name="attsearch">
		 	<xsl:with-param name="tpos" select="$tpos"/>
		    <xsl:with-param name="dpattpos" select="$dpattpos"/>
		    <xsl:with-param name="ttablepos" select="$ttablepos"/>
		    <xsl:with-param name="tname" select="$tname"/>
		    <xsl:with-param name="ctable" select="$ctable"/>
		    <xsl:with-param name="targettable" select="$targettable"/>
		    <xsl:with-param name="targetatt" select="1"/>
		 </xsl:call-template>
</xsl:template>

<!-- Parcouring each attribute in a table $targettable with the goal of searching for an attribut match between an attribut of the pattern at the position $dpattpos with any attribute in all tables of the instance. The goal being to return a updated table name -->
<xsl:template name="attsearch">
	<xsl:param name="tpos"/>
	<xsl:param name="dpattpos"/>
	<xsl:param name="ttablepos"/>
	<xsl:param name="tname"/>
	<xsl:param name="ctable"/>
	<xsl:param name="targettable"/>
	<xsl:param name="targetatt"/>
		<xsl:choose>
			<!-- A match is found between the pattern attribute in position $dpattpos of the table $ctable with the attribute in the database instance of position $targetatt in table $targettable of position $ttablepos. If an atribute is found within a table we can automatically skip the rest of the attributes and move to the next table -->
			<xsl:when test="$ctable//column[position() = $dpattpos]/@var = $targettable//column[position() = $targetatt]/@var">
				<!-- A test to prevent redundancy in table name (to avoid names such as 'R-Employee-Employee-Employee') -->	
				<xsl:if test="contains($tname, $targettable/@name)">
					<xsl:variable name="newtname" select="concat($tname, '-', $targettable/@name)"/>
					<xsl:choose>
						<!-- If we have a match in the last table, we are done with this pattern attribute and move to the next one -->
						<xsl:when test="$ttablepos = count($dbex//table)">
							<xsl:call-template name="table">
			                	<xsl:with-param name="tpos" select="$tpos"/>
			                	<xsl:with-param name="dpattpos" select="$dpattpos + 1"/>
			                	<xsl:with-param name="tname" select="$tname"/>
			                </xsl:call-template>
						</xsl:when>
						<!-- Otherwise we move to the next instance table -->
						<xsl:otherwise>
							<xsl:call-template name="tablesearch">
								<xsl:with-param name="tpos" select="$tpos"/>
					            <xsl:with-param name="dpattpos" select="$dpattpos"/>
					            <xsl:with-param name="ttablepos" select="$ttablepos +1"/>
					            <xsl:with-param name="tname" select="$tname"/>
					            <xsl:with-param name="ctable" select="$ctable"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<!-- The opposite of the previous test. Somehow, these condition didn't worked using <xsl:choose> -->
				<xsl:if test="not(contains($tname, $targettable/@name))">
					<xsl:variable name="newtname" select="concat($tname, '-', $targettable/@name)"/>
					<xsl:choose>
						<!-- If we have a match in the last table, we are done with this pattern attribute and move to the next one -->
						<xsl:when test="$ttablepos = count($dbex//table)">
							<xsl:call-template name="table">
			                	<xsl:with-param name="tpos" select="$tpos"/>
			                	<xsl:with-param name="dpattpos" select="$dpattpos + 1"/>
			                	<xsl:with-param name="tname" select="$newtname"/>
			                </xsl:call-template>
						</xsl:when>
						<!-- Otherwise we move to the next instance table -->
						<xsl:otherwise>
							<xsl:call-template name="tablesearch">
								<xsl:with-param name="tpos" select="$tpos"/>
					            <xsl:with-param name="dpattpos" select="$dpattpos"/>
					            <xsl:with-param name="ttablepos" select="$ttablepos +1"/>
					            <xsl:with-param name="tname" select="$newtname"/>
					            <xsl:with-param name="ctable" select="$ctable"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>		
			</xsl:when>
			<xsl:otherwise>
				<!-- No match found -->
				<xsl:choose>
					<!-- As long as the attribute indice ($targetatt) of the current instance table ($targettable) is lower than the total number of attribute of that table, we recursively move to the next attribute.  -->
					<xsl:when test="$targetatt &lt; count($targettable//column)">
						<xsl:call-template name="attsearch">
						 	<xsl:with-param name="tpos" select="$tpos"/>
						    <xsl:with-param name="dpattpos" select="$dpattpos"/>
						    <xsl:with-param name="ttablepos" select="$ttablepos"/>
						    <xsl:with-param name="tname" select="$tname"/>
						    <xsl:with-param name="ctable" select="$ctable"/>
						    <xsl:with-param name="targettable" select="$targettable"/>
						    <xsl:with-param name="targetatt" select="$targetatt+1"/>
						 </xsl:call-template>
					</xsl:when>
					<!-- Otherwise, if we reached the last attribute of an instance table -->
					<xsl:otherwise>
						<xsl:choose>
							<!-- If its the last table we are done -->
							<xsl:when test="$ttablepos = count($dbex//table)">
								<xsl:call-template name="table">
				                	<xsl:with-param name="tpos" select="$tpos"/>
				                	<xsl:with-param name="dpattpos" select="$dpattpos + 1"/>
				                	<xsl:with-param name="tname" select="$tname"/>
				                </xsl:call-template>
							</xsl:when>
							<!-- Otherwise we move to the next table -->
							<xsl:otherwise>
								<xsl:call-template name="tablesearch">
									<xsl:with-param name="tpos" select="$tpos"/>
						            <xsl:with-param name="dpattpos" select="$dpattpos"/>
						            <xsl:with-param name="ttablepos" select="$ttablepos +1"/>
						            <xsl:with-param name="tname" select="$tname"/>
						            <xsl:with-param name="ctable" select="$ctable"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
</xsl:template>

<xsl:template name="attcolumn">
	<xsl:param name="tpos"/>
	<xsl:param name="ctable"/>
	<xsl:param name="initattind"/>
	<xsl:param name="finalattind"/>
	<xsl:param name="targetind"/>
	<xsl:param name="targettablepos"/>
	<xsl:param name="atttype"/>
		<xsl:variable name="sourcecolumn" select="$ctable//column[position() = $initattind]"/>
		<xsl:variable name="targetcolumn" select="$dbex//table[position() = $targettablepos]//column[position() = $targetind]"/>
		<xsl:variable name="sourcevar" select="$sourcecolumn/@var"/>
		<xsl:variable name="targetvar" select="$targetcolumn/@var"/>
		<xsl:choose>
			<!-- A match is found between the pattern atribute $sourcevar and the instance attribute $targetvar. Somehow, a simple equal operation doesn't work properly when comparing the strings, therefore the formula is a bit more complex that it usually is. -->
			<xsl:when test="contains($sourcevar, $targetvar) and (string-length($sourcevar) = string-length($targetvar))">
				<xsl:call-template name="redundantAvoidance">
					<xsl:with-param name="tpos" select="$tpos"/>
						<xsl:with-param name="ctable" select="$ctable"/>
						<xsl:with-param name="initattind" select="$initattind"/>
						<xsl:with-param name="finalattind" select="$finalattind"/>
						<xsl:with-param name="targetind" select="$targetind"/>
						<xsl:with-param name="targettablepos" select="$targettablepos"/>
						<xsl:with-param name="atttype" select="$atttype"/>
						<xsl:with-param name="backwardtablepos" select="1"/>
						<xsl:with-param name="backwardattpos" select="1"/>
				</xsl:call-template>
				<!--
				<test>
					WHY YOU REPEAT YOURSELF?
					Source Table: <xsl:value-of select="$ctable/@name"/>
					Target Table: <xsl:value-of select="$dbex//table[position() = $targettablepos]/@name"/>
					VAR: <xsl:value-of select="$sourcevar"/>
					NAME: <xsl:value-of select="$targetcolumn/@name"/>
				</test>
				-->
				
			</xsl:when>
			<!-- No match found -->
			<xsl:otherwise>
				<xsl:choose>
					<!-- We reached the last attribute of a instance table -->
					<xsl:when test="$targetind = count($dbex//table[position() = $targettablepos]//column)">
						<xsl:choose>
							<!-- If it is the last table in the instance database and the current pattern attribute isn't the last one of its table, we move to the next pattern atribute -->
							<xsl:when test="$targettablepos = count($dbex//table)">
								<xsl:if test="$initattind &lt; count($ctable//column)">
									<xsl:call-template name="attcolumn">
										<xsl:with-param name="tpos" select="$tpos"/>
										<xsl:with-param name="ctable" select="$ctable"/>
										<xsl:with-param name="initattind" select="$initattind+1"/>
										<xsl:with-param name="finalattind" select="$finalattind"/>
										<xsl:with-param name="targetind" select="1"/>
										<xsl:with-param name="targettablepos" select="1 "/>
										<xsl:with-param name="atttype" select="$atttype"/>
									</xsl:call-template>
								</xsl:if>
							</xsl:when>
							<!-- Otherwise we move to the next instance table -->
							<xsl:otherwise>
								<xsl:call-template name="attcolumn">
									<xsl:with-param name="tpos" select="$tpos"/>
									<xsl:with-param name="ctable" select="$ctable"/>
									<xsl:with-param name="initattind" select="$initattind"/>
									<xsl:with-param name="finalattind" select="$finalattind"/>
									<xsl:with-param name="targetind" select="1"/>
									<xsl:with-param name="targettablepos" select="$targettablepos + 1 "/>
									<xsl:with-param name="atttype" select="$atttype"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>		
					</xsl:when>
					<!-- Otherwise we move to the next instance attribute -->
					<xsl:otherwise>
						<xsl:call-template name="attcolumn">
							<xsl:with-param name="tpos" select="$tpos"/>
							<xsl:with-param name="ctable" select="$ctable"/>
							<xsl:with-param name="initattind" select="$initattind"/>
							<xsl:with-param name="finalattind" select="$finalattind"/>
							<xsl:with-param name="targetind" select="$targetind+1"/>
							<xsl:with-param name="targettablepos" select="$targettablepos"/>
							<xsl:with-param name="atttype" select="$atttype"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
</xsl:template>

<xsl:template name="redundantAvoidance">
	<xsl:param name="tpos"/>
	<xsl:param name="ctable"/>
	<xsl:param name="initattind"/>
	<xsl:param name="finalattind"/>
	<xsl:param name="targetind"/>
	<xsl:param name="targettablepos"/>
	<xsl:param name="atttype"/>
	<xsl:param name="backwardtablepos"/>
	<xsl:param name="backwardattpos"/>

		<xsl:variable name="sourcecolumn" select="$ctable//column[position() = $initattind]"/>
		<xsl:variable name="targetcolumn" select="$dbex//table[position() = $targettablepos]//column[position() = $targetind]"/>
		<xsl:variable name="backwardcolumn" select="$dbex//table[position() = $backwardtablepos]//column[position() = $backwardattpos]"/>
		<xsl:variable name="targetvar" select="$targetcolumn/@var"/>
		<xsl:variable name="backvar" select="$backwardcolumn/@var"/>
		<xsl:variable name="targetname" select="$targetcolumn/@name"/>
		<xsl:variable name="backname" select="$backwardcolumn/@name"/>
		
		<xsl:choose>
			<!-- Whenever the backward table parcour reaches the current table index, we knows that no attribute with a same var name couple was found previously -->
			<xsl:when test="$targettablepos = $backwardtablepos">
				<xsl:choose>
					<!-- If the instance attribute is the last one of its table, we write a new column we move to the next pattern attribute -->
					<xsl:when test="$targetind = count($dbex//table[position() = $targettablepos]//column)">
						<column var="{$sourcecolumn/@var}">
							<!-- The reason for the $attype parameter and this condition is that this template is used later to match attributes in constraints. We differentiate because when want to matche attribute from pattern table and instance table, we want to update their ID. This is the reason for the parameter $finalattind.  -->
							<xsl:if test="$atttype = 'table'">
								<xsl:attribute name="id">
									rel<xsl:value-of select="$tpos"/>att<xsl:value-of select="$finalattind"/>
								</xsl:attribute>
							</xsl:if>
							<xsl:if test="$atttype != 'table'">
								<xsl:attribute name="id"/>
							</xsl:if>
							<xsl:attribute name="name">
								<xsl:value-of select="$targetcolumn/@name"/>
							</xsl:attribute>
							<xsl:attribute name="nullable">
								<xsl:value-of select="$targetcolumn/@nullable"/>
							</xsl:attribute>
						</column>
						<xsl:if test="$initattind &lt; count($ctable//column)">
							<xsl:call-template name="attcolumn">
								<xsl:with-param name="tpos" select="$tpos"/>
								<xsl:with-param name="ctable" select="$ctable"/>
								<xsl:with-param name="initattind" select="$initattind+1"/>
								<xsl:with-param name="finalattind" select="$finalattind +1"/>
								<xsl:with-param name="targetind" select="1"/>
								<xsl:with-param name="targettablepos" select="1"/>
								<xsl:with-param name="atttype" select="$atttype"/>
							</xsl:call-template>
						</xsl:if>
					</xsl:when>
					<!-- Otherwise, we move to the next attribute in the instance table -->
					<xsl:otherwise>
						<column var="{$sourcecolumn/@var}">
							<xsl:if test="$atttype = 'table'">
								<xsl:attribute name="id">
									rel<xsl:value-of select="$tpos"/>att<xsl:value-of select="$finalattind"/>
								</xsl:attribute>
							</xsl:if>
							<xsl:if test="$atttype != 'table'">
								<xsl:attribute name="id"/>
							</xsl:if>
							<xsl:attribute name="name">
								<xsl:value-of select="$targetcolumn/@name"/>
							</xsl:attribute>
							<xsl:attribute name="nullable">
								<xsl:value-of select="$targetcolumn/@nullable"/>
							</xsl:attribute>
						</column>
						<xsl:call-template name="attcolumn">
							<xsl:with-param name="tpos" select="$tpos"/>
							<xsl:with-param name="ctable" select="$ctable"/>
							<xsl:with-param name="initattind" select="$initattind"/>
							<xsl:with-param name="finalattind" select="$finalattind+1"/>
							<xsl:with-param name="targetind" select="$targetind+1"/>
							<xsl:with-param name="targettablepos" select="$targettablepos"/>
							<xsl:with-param name="atttype" select="$atttype"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!-- Otherwise, we parcour the previously seen table and check if the curent couple targevar targetname existed before. If not we keep on parcouring each attributes of each table below the current targetpos. If it is, then we move to the next target and manage every possible case -->
			
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="(contains($backvar, $targetvar) and (string-length($backvar) = string-length($targetvar))) and (contains($backname, $targetname) and (string-length($backname) = string-length($targetname)))">
						<xsl:choose>
							<!-- We reached the last attribute of a instance table -->
							<xsl:when test="$targetind = count($dbex//table[position() = $targettablepos]//column)">
								<xsl:choose>
									<!-- If it is the last table in the instance database and the current pattern attribute isn't the last one of its table, we move to the next pattern atribute -->
									<xsl:when test="$targettablepos = count($dbex//table)">
										<xsl:if test="$initattind &lt; count($ctable//column)">
											<xsl:call-template name="attcolumn">
												<xsl:with-param name="tpos" select="$tpos"/>
												<xsl:with-param name="ctable" select="$ctable"/>
												<xsl:with-param name="initattind" select="$initattind+1"/>
												<xsl:with-param name="finalattind" select="$finalattind"/>
												<xsl:with-param name="targetind" select="1"/>
												<xsl:with-param name="targettablepos" select="1 "/>
												<xsl:with-param name="atttype" select="$atttype"/>
											</xsl:call-template>
										</xsl:if>
									</xsl:when>
									<!-- Otherwise we move to the next instance table -->
									<xsl:otherwise>
										<xsl:call-template name="attcolumn">
											<xsl:with-param name="tpos" select="$tpos"/>
											<xsl:with-param name="ctable" select="$ctable"/>
											<xsl:with-param name="initattind" select="$initattind"/>
											<xsl:with-param name="finalattind" select="$finalattind"/>
											<xsl:with-param name="targetind" select="1"/>
											<xsl:with-param name="targettablepos" select="$targettablepos + 1 "/>
											<xsl:with-param name="atttype" select="$atttype"/>
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
							</xsl:when>
							<!-- Otherwise we move to the next instance attribute -->
							<xsl:otherwise>
								<xsl:call-template name="attcolumn">
									<xsl:with-param name="tpos" select="$tpos"/>
									<xsl:with-param name="ctable" select="$ctable"/>
									<xsl:with-param name="initattind" select="$initattind"/>
									<xsl:with-param name="finalattind" select="$finalattind"/>
									<xsl:with-param name="targetind" select="$targetind+1"/>
									<xsl:with-param name="targettablepos" select="$targettablepos"/>
									<xsl:with-param name="atttype" select="$atttype"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>

					</xsl:when>
					
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$backwardattpos &gt;= count($dbex//table[position() = $backwardtablepos]//column)">
								
								<xsl:call-template name="redundantAvoidance">
									<xsl:with-param name="tpos" select="$tpos"/>
									<xsl:with-param name="ctable" select="$ctable"/>
									<xsl:with-param name="initattind" select="$initattind"/>
									<xsl:with-param name="finalattind" select="$finalattind"/>
									<xsl:with-param name="targetind" select="$targetind"/>
									<xsl:with-param name="targettablepos" select="$targettablepos"/>
									<xsl:with-param name="atttype" select="$atttype"/>
									<xsl:with-param name="backwardtablepos" select="$backwardtablepos+1"/>
									<xsl:with-param name="backwardattpos" select="1"/>
								</xsl:call-template>
							
							</xsl:when>
							<xsl:otherwise>
								
								<xsl:call-template name="redundantAvoidance">
									<xsl:with-param name="tpos" select="$tpos"/>
										<xsl:with-param name="ctable" select="$ctable"/>
										<xsl:with-param name="initattind" select="$initattind"/>
										<xsl:with-param name="finalattind" select="$finalattind"/>
										<xsl:with-param name="targetind" select="$targetind"/>
										<xsl:with-param name="targettablepos" select="$targettablepos"/>
										<xsl:with-param name="atttype" select="$atttype"/>
										<xsl:with-param name="backwardtablepos" select="$backwardtablepos"/>
										<xsl:with-param name="backwardattpos" select="$backwardattpos+1"/>
								</xsl:call-template>
							
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				
				</xsl:choose>
			</xsl:otherwise>

		</xsl:choose>

		
	
</xsl:template>

<!-- Here we want to update the constraints present in the database pattern. This include updating the name of the source and target tables and filling each var attribute with instance attribute with the same var -->
<xsl:template name="constraint">
	<xsl:param name="cpos"/>
    <xsl:param name="csattpos"/>
    <xsl:param name="csname"/>
    <xsl:param name="ctattpos"/>
    <xsl:param name="ctname"/>
    	<xsl:variable name="currcons" select="$dp2//constraints/constraint[position() = $cpos]"/>
    	<!-- In the database pattern we have several additional constraints that do not exists in instances. For now we ignore them but there might be additional work in exploiting them eventually -->
    	<xsl:if test="(not($currcons/@type = 'CONS')) and (not($currcons/@type = 'IS_NOT')) and (not($currcons/@type = 'SUBSET')) ">
	    	<xsl:choose>
	    		<!-- Once we found the new names of both the source and target table of this constraint after parcouring through each instance table from each constraint pattern attribute, we write what we found and move to the attributes. -->
	    		<xsl:when test="($csattpos > count($currcons/source//column)) and ($ctattpos > count($currcons/target//column))">
	    			<constraint name="{$currcons/@name}" id="{$currcons/@id}" type="{$currcons/@type}">
						<source name ="" id ="" table="{$csname}">
							<columns>
								<xsl:call-template name="attcolumn">
									<xsl:with-param name="tpos" select="1"/>
									<xsl:with-param name="ctable" select="$dp2//constraints/constraint[@name = $currcons/@name]/source"/>
									<xsl:with-param name="initattind" select="1"/>
									<xsl:with-param name="finalattind" select="1"/>
									<xsl:with-param name="targetind" select="1"/>
									<xsl:with-param name="targettablepos" select="1"/>
									<xsl:with-param name="atttype" select="'constraint'"/>
								</xsl:call-template>						
							</columns>
						</source>
						<target name ="" id ="" table="{$ctname}">
							<columns>
								<xsl:call-template name="attcolumn">
									<xsl:with-param name="tpos" select="1"/>
									<xsl:with-param name="ctable" select="$dp2//constraints/constraint[@name = $currcons/@name]/target"/>
									<xsl:with-param name="initattind" select="1"/>
									<xsl:with-param name="finalattind" select="1"/>
									<xsl:with-param name="targetind" select="1"/>
									<xsl:with-param name="targettablepos" select="1"/>
									<xsl:with-param name="atttype" select="'constraint'"/>
								</xsl:call-template>						
							</columns>
						</target>
					</constraint>
					<xsl:if test="$cpos &lt; count($dp2//constraint)">
						<xsl:call-template name="constraint">
		                	<xsl:with-param name="cpos" select="$cpos+1"/>
		                	<xsl:with-param name="csattpos" select="1"/>
		                	<xsl:with-param name="csname" select="$dp2//constraint[position() = $cpos+1]/source/@table"/>
		                	<xsl:with-param name="ctattpos" select="1"/>
		                	<xsl:with-param name="ctname" select="$dp2//constraint[position() = $cpos+1]/target/@table"/>
		                </xsl:call-template>
					</xsl:if>
	    		</xsl:when>
	    		<xsl:when test="($csattpos > count($currcons/source//column)) and ($ctattpos &lt;= count($currcons/target//column))">
	    			<xsl:call-template name="consnamesearch">
	    				<xsl:with-param name="cpos" select="$cpos"/>
		                <xsl:with-param name="csattpos" select="$csattpos"/>
		                <xsl:with-param name="csname" select="$csname"/>
		                <xsl:with-param name="ctattpos" select="$ctattpos"/>
		                <xsl:with-param name="ctname" select="$ctname"/>
		                <xsl:with-param name="subcons" select="$currcons/target"/>
		                <xsl:with-param name="tsearchpos" select="1"/>
		                <xsl:with-param name="tsearchatt" select="1"/>
	    			</xsl:call-template>
	    		</xsl:when>
	    		<xsl:otherwise>
	    			<xsl:call-template name="consnamesearch">
	    				<xsl:with-param name="cpos" select="$cpos"/>
		                <xsl:with-param name="csattpos" select="$csattpos"/>
		                <xsl:with-param name="csname" select="$csname"/>
		                <xsl:with-param name="ctattpos" select="$ctattpos"/>
		                <xsl:with-param name="ctname" select="$ctname"/>
		                <xsl:with-param name="subcons" select="$currcons/source"/>
		                <xsl:with-param name="tsearchpos" select="1"/>
		                <xsl:with-param name="tsearchatt" select="1"/>
	    			</xsl:call-template>
	    		</xsl:otherwise>
	    	</xsl:choose>
    	</xsl:if>
</xsl:template>
	
<xsl:template name="consnamesearch">
	<xsl:param name="cpos"/>
    <xsl:param name="csattpos"/>
    <xsl:param name="csname"/>
    <xsl:param name="ctattpos"/>
    <xsl:param name="ctname"/>
    <xsl:param name="subcons"/>
    <xsl:param name="tsearchpos"/>
    <xsl:param name="tsearchatt"/>
   		<xsl:variable name="tablesearch" select="$dbex//table[position() = $tsearchpos]"/>
   		
   		<xsl:choose>
   			<xsl:when test="$subcons//column[position() = 1]/@var = $tablesearch//column[position() = $tsearchatt]/@var">
   				<xsl:choose>
   					<xsl:when test="$dp2//constraint[position() = $cpos]/source/@table = $dp2//constraint[position() = $cpos]/target/@table">
   						<xsl:choose>
		   					<xsl:when test="name($subcons) = 'source'">
		   						<xsl:if test="contains($csname, $tablesearch/@name)">
		   							<xsl:call-template name="constraint">
							            <xsl:with-param name="cpos" select="$cpos"/>
							            <xsl:with-param name="csattpos" select="count($subcons//column) + 1"/>
							            <xsl:with-param name="csname" select="$csname"/>
							            <xsl:with-param name="ctattpos" select="1"/>
							            <xsl:with-param name="ctname" select="$ctname"/>
							        </xsl:call-template>
		   						</xsl:if>
		   						<xsl:if test="not(contains($csname, $tablesearch/@name))">
		   							<xsl:call-template name="constraint">
							            <xsl:with-param name="cpos" select="$cpos"/>
							            <xsl:with-param name="csattpos" select="count($subcons//column) + 1"/>
							            <xsl:with-param name="csname" select="concat($csname, '-', $tablesearch/@name)"/>
							            <xsl:with-param name="ctattpos" select="1"/>
							            <xsl:with-param name="ctname" select="concat($ctname, '-', $tablesearch/@name)"/>
							        </xsl:call-template>
		   						</xsl:if>		
						    </xsl:when>
						    <xsl:otherwise>
						    	<xsl:if test="contains($ctname, $tablesearch/@name)">
						    		<xsl:call-template name="constraint">
							            <xsl:with-param name="cpos" select="$cpos"/>
							            <xsl:with-param name="csattpos" select="$csattpos"/>
							            <xsl:with-param name="csname" select="$csname"/>
							            <xsl:with-param name="ctattpos" select="count($subcons//column) + 1"/>
							            <xsl:with-param name="ctname" select="$ctname"/>
							        </xsl:call-template>
		   						</xsl:if>
		   						<xsl:if test="not(contains($ctname, $tablesearch/@name))">
		   							<xsl:call-template name="constraint">
							            <xsl:with-param name="cpos" select="$cpos"/>
							            <xsl:with-param name="csattpos" select="$csattpos"/>
							            <xsl:with-param name="csname" select="concat($csname, '-', $tablesearch/@name)"/>
							            <xsl:with-param name="ctattpos" select="count($subcons//column) + 1"/>
							            <xsl:with-param name="ctname" select="concat($ctname, '-', $tablesearch/@name)"/>
							        </xsl:call-template>
		   						</xsl:if>			    	
						    </xsl:otherwise>
						</xsl:choose>
   					</xsl:when>
   					<xsl:otherwise>
   						<xsl:choose>
		   					<xsl:when test="name($subcons) = 'source'">
		   						<xsl:if test="contains($csname, $tablesearch/@name)">
		   							<xsl:call-template name="constraint">
							            <xsl:with-param name="cpos" select="$cpos"/>
							            <xsl:with-param name="csattpos" select="count($subcons//column) + 1"/>
							            <xsl:with-param name="csname" select="$csname"/>
							            <xsl:with-param name="ctattpos" select="1"/>
							            <xsl:with-param name="ctname" select="$ctname"/>
							        </xsl:call-template>
		   						</xsl:if>
		   						<xsl:if test="not(contains($csname, $tablesearch/@name))">
		   							<xsl:call-template name="constraint">
							            <xsl:with-param name="cpos" select="$cpos"/>
							            <xsl:with-param name="csattpos" select="count($subcons//column) + 1"/>
							            <xsl:with-param name="csname" select="concat($csname, '-', $tablesearch/@name)"/>
							            <xsl:with-param name="ctattpos" select="1"/>
							            <xsl:with-param name="ctname" select="$ctname"/>
							        </xsl:call-template>
		   						</xsl:if>
				   				
						    </xsl:when>
						    <xsl:otherwise>
						    	<xsl:if test="contains($ctname, $tablesearch/@name)">
						    		<xsl:call-template name="constraint">
							            <xsl:with-param name="cpos" select="$cpos"/>
							            <xsl:with-param name="csattpos" select="$csattpos"/>
							            <xsl:with-param name="csname" select="$csname"/>
							            <xsl:with-param name="ctattpos" select="count($subcons//column) + 1"/>
							            <xsl:with-param name="ctname" select="$ctname"/>
							        </xsl:call-template>
		   						</xsl:if>
		   						<xsl:if test="not(contains($ctname, $tablesearch/@name))">
		   							<xsl:call-template name="constraint">
							            <xsl:with-param name="cpos" select="$cpos"/>
							            <xsl:with-param name="csattpos" select="$csattpos"/>
							            <xsl:with-param name="csname" select="$csname"/>
							            <xsl:with-param name="ctattpos" select="count($subcons//column) + 1"/>
							            <xsl:with-param name="ctname" select="concat($ctname, '-', $tablesearch/@name)"/>
							        </xsl:call-template>
		   						</xsl:if>
						    	
						    </xsl:otherwise>
						</xsl:choose>
   					</xsl:otherwise>
   				</xsl:choose>
   			</xsl:when>
   			<xsl:otherwise>
   				<xsl:choose>
   					<xsl:when test="$tsearchatt = count($tablesearch//column)">
   						<xsl:if test="$tsearchpos &lt; count($dbex//table)">
   							<xsl:call-template name="consnamesearch">
					    		<xsl:with-param name="cpos" select="$cpos"/>
						        <xsl:with-param name="csattpos" select="$csattpos"/>
						        <xsl:with-param name="csname" select="$csname"/>
						        <xsl:with-param name="ctattpos" select="$ctattpos"/>
						        <xsl:with-param name="ctname" select="$ctname"/>
						        <xsl:with-param name="subcons" select="$subcons"/>
						        <xsl:with-param name="tsearchpos" select="$tsearchpos +1"/>
						        <xsl:with-param name="tsearchatt" select="1"/>
					    	</xsl:call-template>
   						</xsl:if>			
   					</xsl:when>
   					<xsl:otherwise>
   						<xsl:call-template name="consnamesearch">
				    		<xsl:with-param name="cpos" select="$cpos"/>
					        <xsl:with-param name="csattpos" select="$csattpos"/>
					        <xsl:with-param name="csname" select="$csname"/>
					        <xsl:with-param name="ctattpos" select="$ctattpos"/>
					        <xsl:with-param name="ctname" select="$ctname"/>
					        <xsl:with-param name="subcons" select="$subcons"/>
					        <xsl:with-param name="tsearchpos" select="$tsearchpos"/>
					        <xsl:with-param name="tsearchatt" select="$tsearchatt + 1"/>
				    	</xsl:call-template>
   					</xsl:otherwise>
   				</xsl:choose>
   			</xsl:otherwise>
   		</xsl:choose>

</xsl:template>

</xsl:stylesheet>
