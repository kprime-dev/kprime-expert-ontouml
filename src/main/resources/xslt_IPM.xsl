<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<!-- XML Input Files -->
<xsl:variable name="dp1" select="document('dbpattern.xml')/*" />
<!--
<xsl:variable name="db" select="document('dbinstance.xml')/*" />
-->
<xsl:variable name="db" select="./*" />

<xsl:template match="/">
    <database>
        <schema>
            <tables>
                <!-- Matching attributes of the instance and pattern -->
                <xsl:apply-templates select="$db" mode="table"/>
            </tables>
            <constraints>
                <!-- Matching constraints of the instance and pattern -->
                <xsl:if test="$db//constraints/constraint">
                    <xsl:apply-templates select="$db" mode="dbconstraint"/> 
                </xsl:if>
            </constraints>
        </schema>
    </database>
</xsl:template>

<!-- For each relation in the instance -->
<xsl:template match="tables/table" mode="table">
    <xsl:variable name="tid" select="@id"/>
    <xsl:variable name="tname" select="@name"/>
    <xsl:variable name="dptname" select="$dp1/*/tables/table[@id = $tid]/@name"/>

    <table var="{$dptname}" id="{@id}" name="{@name}">
        <columns>
            <!-- We propagate information about the relation (id and name) to the next template -->
            <xsl:apply-templates select="$db/*/tables/table[@id = $tid]" mode="attparse">
                <xsl:with-param name="tid" select="$tid"/>
                <xsl:with-param name="tname" select="$tname"/>
            </xsl:apply-templates>
        </columns>
    </table>

</xsl:template>

<!-- For each attribute in a relation of the instance -->
<xsl:template match="columns/column" mode="attparse">
    <xsl:param name="tid"/>
    <xsl:param name="tname"/>
        <!-- We propagate information on the relation as well as on the current attribute (attcolumn) to the next template -->
        <xsl:call-template name="attsearch">
                <xsl:with-param name="tid" select="$tid"/>
                <xsl:with-param name="tname" select="$tname"/>
                <xsl:with-param name="attcolumn" select="."/>
                <xsl:with-param name="consind" select="1"/>
            </xsl:call-template>
</xsl:template>

<!-- Looking in the database instance for constraints over one relation -->
<xsl:template name="attsearch">
    <xsl:param name="tid"/>
    <xsl:param name="tname"/>
    <xsl:param name="attcolumn"/>
    <xsl:param name="consind"/>
        <xsl:if test="not($db//constraint)">
            <column var="{$dp1//table[@id = $tid]/columns/column[position() = count($dp1//table[@id = $tid]/columns/column)]/@var}" id="{$attcolumn/@id}" name="{$attcolumn/@name}"/>
        </xsl:if>
        <xsl:variable name="currcons" select="$db/*/constraints/constraint[position() = $consind]"/>
        <xsl:choose>
            <!-- The current constraint applies over a chosen relation. -->
            <xsl:when test="$currcons/target/@table = $tname or $currcons/source/@table = $tname">
                <xsl:call-template name="attincons">
                    <xsl:with-param name="tid" select="$tid"/>
                    <xsl:with-param name="tname" select="$tname"/>
                    <xsl:with-param name="attcolumn" select="$attcolumn"/>
                    <xsl:with-param name="consind" select="$consind"/>
                    <xsl:with-param name="currcons" select="$currcons"/>
                    <xsl:with-param name="currattind" select="1"/>
                </xsl:call-template>
            </xsl:when>
            <!-- Otherwise, we recursively move to the next constraint -->
            <xsl:otherwise>
                <xsl:if test="$consind &lt; count($db/*/constraints/constraint)">
                    <xsl:call-template name="attsearch">
                        <xsl:with-param name="tid" select="$tid"/>
                        <xsl:with-param name="tname" select="$tname"/>
                        <xsl:with-param name="attcolumn" select="$attcolumn"/>
                        <xsl:with-param name="consind" select="$consind + 1"/>
                    </xsl:call-template>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>           
</xsl:template>

<xsl:template name="attincons">
    <xsl:param name="tid"/>
    <xsl:param name="tname"/>
    <xsl:param name="attcolumn"/>
    <xsl:param name="consind"/>
    <xsl:param name="currcons"/>
    <xsl:param name="currattind"/>

        <xsl:variable name="consnumb" select="count($currcons//column)"/>
        <!-- We use an indice ($currattind) to browse the constraint's attribute. Because they are divided in source and target, one way to browse them linearly is by using their position in abstract tree without these division. This abstract tree is '$currcons//column' and each column has its own unique position within it. -->
        <xsl:for-each select="$currcons//column">

            <xsl:if test="position() = $currattind">
                <xsl:choose>
                    <!-- Comparing current attribute name within the loop with the one in parameter -->
                    <xsl:when test="$attcolumn/@name = @name">
                        <!--
                        <xsl:apply-templates select="$dp1//constraints/constraint[@type = $currcons/@type]" mode="dpconsmatch">
                            <xsl:with-param name="currcons" select="$currcons"/>
                            <xsl:with-param name="attcolumn" select="$attcolumn"/>
                            <xsl:with-param name="indbytype" select="$consind"/>
                            <xsl:with-param name="currattind" select="$currattind"/>
                        </xsl:apply-templates> 
                        -->
                        <xsl:call-template name="dpconsmatch">
                            <xsl:with-param name="currcons" select="$currcons"/>
                            <xsl:with-param name="attcolumn" select="$attcolumn"/>
                            <xsl:with-param name="indbytype" select="$consind"/>
                            <xsl:with-param name="currattind" select="$currattind"/>
                            <xsl:with-param name="targetpos" select="1"/>
                            <xsl:with-param name="trigger" select="'no'"/>
                        </xsl:call-template>  
                    </xsl:when>
                    <!-- No match -->
                    <xsl:otherwise>
                        <xsl:choose>
                            <!-- If the tested attribute is the last one in the constraint -->
                            <xsl:when test="position() = $consnumb">

                                <xsl:variable name="conslist" select="$db/*/constraints/constraint[source/@table = $tname or target/@table = $tname]"/>
                                <!-- We are trying to determined if the attribute in parameter exists in the constraints. We are at the last attribute of a constraint and if this is the last constraint we will know if the attribute in parameter exists or not. We create a new set composed of every constraints containing the name of a chosen relation as a source or as a target ($conslist). If the name of the constraint at the end of that list is equal to the name of the curent constraint, then this is the last constraint containing attributes from a chosen relation. -->
                                <xsl:for-each select="$conslist">
                                    <xsl:if test="(@name = $currcons/@name) and (position() = count($conslist))">
                                        <!-- Because the current attribute doesn't appear inside any constraints, it is difficult to match it precisely with anything in the database pattern. In the end, we match the last attribute of a relation with the same id as $tid to the current attribute. This approach also restrict the writing of a dataase pattern since any attribute not present in constraints, the REST, has to be the last element of a relation. -->
                                        <!--
                                        <column var="{$dp1/*/tables/table[@id = $tid]/columns/column[position() = count($dp1/*/tables/table[@id = $tid]/columns/column)]/@var}" id="{$attcolumn/@id}" name="{$attcolumn/@name}" nullable="{$attcolumn/@nullable}"/>
                                        -->
                                        <xsl:for-each select="$dp1//table">
                                            <xsl:variable name="currtid" select="@id"/>
                                            <column var="{$dp1/*/tables/table[@id = $currtid]/columns/column[position() = count($dp1/*/tables/table[@id = $tid]/columns/column)]/@var}" id="{$attcolumn/@id}" name="{$attcolumn/@name}" nullable="{$attcolumn/@nullable}" match="s{$tid}t{@id}"/>
                                        </xsl:for-each>
                                    </xsl:if>
                                </xsl:for-each>
                            </xsl:when>
                            <!-- If not, we recursively move to the next attribute -->
                            <xsl:otherwise>
                                <xsl:if test="$currattind &lt; $consnumb">
                                    <xsl:call-template name="attincons">
                                        <xsl:with-param name="tid" select="$tid"/>
                                        <xsl:with-param name="tname" select="$tname"/>
                                        <xsl:with-param name="attcolumn" select="$attcolumn"/>
                                        <xsl:with-param name="consind" select="$consind"/>
                                        <xsl:with-param name="currcons" select="$currcons"/>
                                        <xsl:with-param name="currattind" select="$currattind +1"/>
                                    </xsl:call-template>
                                </xsl:if>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
        </xsl:for-each>
</xsl:template>

<xsl:template name="dpconsmatch">
    <xsl:param name="currcons"/>
    <xsl:param name="attcolumn"/>
    <xsl:param name="indbytype"/>
    <xsl:param name="currattind"/>
    <xsl:param name="targetpos"/>
    <xsl:param name="trigger"/>
        <xsl:variable name="targetcons" select="$dp1//constraints/constraint[position() = $targetpos]"/>
        <!--
        <test>
            TRIGGER: <xsl:value-of select="$trigger"/>
            SNAME: <xsl:value-of select="$currcons/@name"/>
            TNAME: <xsl:value-of select="$targetcons/@name"/>
            TPOS: <xsl:value-of select="$targetpos"/>
            TCOUNT: <xsl:value-of select="count($dp1//constraint)"/>
        </test>
        -->
        <xsl:choose>
            <xsl:when test="$targetcons/@type = $currcons/@type">    
                <xsl:variable name="insource" select="count($currcons/source/columns/column)"/>
                <xsl:variable name="currname" select="$targetcons/@name"/>
                <xsl:for-each select="$dp1/*/constraints/constraint[@type = $currcons/@type]">
                    <xsl:if test="@name = $currname">
                        <xsl:choose>
                            <xsl:when test="$currattind &lt;= $insource">
                                <column var="{./source/columns/column/@var}" id="{$attcolumn/@id}" name="{$attcolumn/@name}" nullable="{$attcolumn/@nullable}" match="s{$indbytype}t{position()}"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <column var="{./target/columns/column/@var}" id="{$attcolumn/@id}" name="{$attcolumn/@name}" nullable="{$attcolumn/@nullable}" match="s{$indbytype}t{position()}"/>
                            </xsl:otherwise>
                        </xsl:choose>                        
                        <xsl:if test="$targetpos &lt; count($dp1//constraint)">
                            <xsl:call-template name="dpconsmatch">
                                <xsl:with-param name="currcons" select="$currcons"/>
                                <xsl:with-param name="attcolumn" select="$attcolumn"/>
                                <xsl:with-param name="indbytype" select="$indbytype"/>
                                <xsl:with-param name="currattind" select="$currattind"/>
                                <xsl:with-param name="targetpos" select="$targetpos+1"/>
                                <xsl:with-param name="trigger" select="'yes'"/>
                            </xsl:call-template>                        
                        </xsl:if>               
                    </xsl:if>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$targetpos = count($dp1//constraint)">
                        <xsl:if test="$trigger = 'no'">
                            <xsl:copy-of select="$attcolumn"/>
                        </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="dpconsmatch">
                            <xsl:with-param name="currcons" select="$currcons"/>
                            <xsl:with-param name="attcolumn" select="$attcolumn"/>
                            <xsl:with-param name="indbytype" select="$indbytype"/>
                            <xsl:with-param name="currattind" select="$currattind"/>
                            <xsl:with-param name="targetpos" select="$targetpos+1"/>
                            <xsl:with-param name="trigger" select="$trigger"/>
                            </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>    
</xsl:template>


<xsl:template match="constraints/constraint" mode="dbconstraint">
    <xsl:variable name="dbcons" select="."/>
    <xsl:variable name="dbtype" select="@type"/>
    <xsl:variable name="dbname" select="@name"/>

    <xsl:for-each select="$db//constraints/constraint[@type = $dbtype]">
        
        <xsl:if test="@name = $dbname">
            <!--
            <xsl:apply-templates select="$dp1/*/constraints/constraint[@type = $dbtype]" mode="dpconstraint">
                <xsl:with-param name="dbcons" select="$dbcons"/>
                <xsl:with-param name="indbytype" select="position()"/>
            </xsl:apply-templates>
        -->
            <xsl:call-template name="dpconstraint">
                <xsl:with-param name="dbcons" select="$dbcons"/>
                <xsl:with-param name="indbytype" select="position()"/>
                <xsl:with-param name="targetpos" select="1"/>
                <xsl:with-param name="trigger" select="'no'"/>
            </xsl:call-template>

        </xsl:if>        
    </xsl:for-each>   
</xsl:template>


<xsl:template name="dpconstraint">
    <xsl:param name="dbcons"/>
    <xsl:param name="indbytype"/>
    <xsl:param name="targetpos"/>
    <xsl:param name="trigger"/>
        <xsl:variable name="dpcons" select="$dp1//constraint[position() = $targetpos]"/>

        <xsl:choose>
            <xsl:when test="$dbcons/@type = $dpcons/@type">
                <xsl:for-each select="$dp1//constraints/constraint[@type=$dpcons/@type]">
                    <xsl:if test="@name = $dpcons/@name">
                        <xsl:call-template name="constraint-matcher">
                            <xsl:with-param name="dbcons" select="$dbcons"/>
                            <xsl:with-param name="dpcons" select="$dpcons"/>
                            <xsl:with-param name="indbytype" select="$indbytype"/>
                            <xsl:with-param name="indbytype2" select="position()"/>
                        </xsl:call-template>
                        <xsl:if test="$targetpos &lt; count($dp1//constraint)">
                            <xsl:call-template name="dpconstraint">
                            <xsl:with-param name="dbcons" select="$dbcons"/>
                            <xsl:with-param name="indbytype" select="$indbytype"/>
                            <xsl:with-param name="targetpos" select="$targetpos+1"/>
                            <xsl:with-param name="trigger" select="'yes'"/>
                        </xsl:call-template>
                        </xsl:if>
                    </xsl:if>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                
                <xsl:choose>
                    <xsl:when test="$targetpos = count($dp1//constraint)">
                        <xsl:if test="$trigger = 'no'">
                            <xsl:copy-of select="$dbcons"/>
                        </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="dpconstraint">
                            <xsl:with-param name="dbcons" select="$dbcons"/>
                            <xsl:with-param name="indbytype" select="$indbytype"/>
                            <xsl:with-param name="targetpos" select="$targetpos+1"/>
                            <xsl:with-param name="trigger" select="$trigger"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            
            </xsl:otherwise>
        </xsl:choose>
        
</xsl:template>

<xsl:template name="constraint-matcher">
    <xsl:param name="dbcons"/>
    <xsl:param name="dpcons"/>
    <xsl:param name="indbytype"/>
    <xsl:param name="indbytype2"/>

        <constraint var="{$dpcons/@name}" id="{$dbcons/@id}" name="{$dbcons/@name}" type="{$dpcons/@type}" match="s{$indbytype}t{$indbytype2}"> 
            <source var="{$dpcons/source/@table}" table="{$dbcons/source/@table}">
                <columns>
                    <xsl:apply-templates select="$dp1/*/constraints/constraint[@name = $dpcons/@name]" mode="source-column">
                        <xsl:with-param name="dbcons" select="$dbcons"/>
                        <xsl:with-param name="dpcons" select="$dpcons"/>
                    </xsl:apply-templates>
                </columns> 
            </source>
            <target var="{$dpcons/target/@table}" table="{$dbcons/target/@table}">
                <columns>
                    <xsl:apply-templates select="$dp1/*/constraints/constraint[@name = $dpcons/@name]" mode="target-column">
                        <xsl:with-param name="dbcons" select="$dbcons"/>
                        <xsl:with-param name="dpcons" select="$dpcons"/>
                    </xsl:apply-templates>
                </columns>
            </target>
        </constraint>

</xsl:template>
<!--
<xsl:template match="constraints/constraint" mode="dpconstraint">
    <xsl:param name="dbcons"/>
    <xsl:param name="indbytype"/>
        <xsl:variable name="dpcons" select="."/>
        <xsl:variable name="dptype" select="@type"/>
        <xsl:variable name="dpname" select="@name"/>


        <xsl:for-each select="$dp1//constraints/constraint[@type = $dptype]">
            <xsl:if test="@name = $dpname">
                <xsl:if test="position() = $indbytype">
                    <xsl:call-template name="constraint-matcher">
                        <xsl:with-param name="dbcons" select="$dbcons"/>
                        <xsl:with-param name="dpcons" select="$dpcons"/>
                    </xsl:call-template>
                </xsl:if>
            </xsl:if>
        </xsl:for-each>
</xsl:template>
-->

<!--
<xsl:template name="constraint-matcher">
    <xsl:param name="dbcons"/>
    <xsl:param name="dpcons"/>
        <constraint var="{$dpcons/@name}" id="{$dbcons/@id}" name="{$dbcons/@name}" type="{$dpcons/@type}"> 
            <source var="{$dpcons/source/@table}" table="{$dbcons/source/@table}">
                <columns>
                    <xsl:apply-templates select="$dp1/*/constraints/constraint[@name = $dpcons/@name]" mode="source-column">
                        <xsl:with-param name="dbcons" select="$dbcons"/>
                        <xsl:with-param name="dpcons" select="$dpcons"/>
                    </xsl:apply-templates>
                </columns> 
            </source>
            <target var="{$dpcons/target/@table}" table="{$dbcons/target/@table}">
                <columns>
                    <xsl:apply-templates select="$dp1/*/constraints/constraint[@name = $dpcons/@name]" mode="target-column">
                        <xsl:with-param name="dbcons" select="$dbcons"/>
                        <xsl:with-param name="dpcons" select="$dpcons"/>
                    </xsl:apply-templates>
                </columns>
            </target>
        </constraint>
</xsl:template>
-->


<!-- Browsing attributes in constraints -->
<xsl:template match="source/columns/column" mode="source-column">
    <xsl:param name="dbcons"/>
    <xsl:param name="dpcons"/>
    <xsl:variable name="var" select="@var"/>

        <xsl:for-each select="$db/*/constraints/constraint[@name = $dbcons/@name]/source/columns/column">
            <!-- <column var="{$var}" id="{@id}" name="{@name}"/> -->
            <xsl:call-template name="column-creation">
                <xsl:with-param name="dp-r-var" select="$var"/>
                <xsl:with-param name="db-r-name" select="@name"/>
                <xsl:with-param name="db-r-id" select="@id"/>
                <xsl:with-param name="dpind" select="1"/>
            </xsl:call-template>
        </xsl:for-each>
</xsl:template>

<xsl:template match="target/columns/column" mode="target-column">
    <xsl:param name="dbcons"/>
    <xsl:param name="dpcons"/>
    <xsl:variable name="var" select="@var"/>
        <xsl:for-each select="$db/*/constraints/constraint[@name = $dbcons/@name]/target/columns/column">
            <!-- <column var="{$var}" id="{@id}" name="{@name}"/> -->
            <xsl:call-template name="column-creation">
                <xsl:with-param name="dp-r-var" select="$var"/>
                <xsl:with-param name="db-r-name" select="@name"/>
                <xsl:with-param name="db-r-id" select="@id"/>
                <xsl:with-param name="dpind" select="1"/>
            </xsl:call-template>
        </xsl:for-each>
</xsl:template>

<xsl:template name="column-creation">
    <xsl:param name="dp-r-var"/>
    <xsl:param name="db-r-name"/>
    <xsl:param name="db-r-id"/>
    <xsl:param name="dpind"/>
        <xsl:choose>
            <xsl:when test="(contains($dp1//constraint[position() = $dpind]/@name,$dp-r-var)) and ($dp1//constraint[position() = $dpind]/@type = 'IS_NOT')">
                <column var="{$dp-r-var}" id="{$db-r-id}" name="{$db-r-name}" nullable="false"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$dpind = count($dp1//constraint)">
                        <column var="{$dp-r-var}" id="{$db-r-id}" name="{$db-r-name}" nullable="true"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="column-creation">
                            <xsl:with-param name="dp-r-var" select="$dp-r-var"/>
                            <xsl:with-param name="db-r-name" select="$db-r-name"/>
                            <xsl:with-param name="db-r-id" select="$db-r-id"/>
                            <xsl:with-param name="dpind" select="$dpind+1"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
        
        <!--    
                    Var="<xsl:value-of select="$dp-r-var"/>"
                    Name="<xsl:value-of select="$db-r-name"/>"
        </column>
        -->
</xsl:template>

</xsl:stylesheet>
