# README

OntoUML, to ontological validation and OWL transformation. 

## USAGE

KPrime integrations, requiring a running instance of KPrime:

    make build-run

KPrime Onto validation:

    http://localhost:7199/validate?prj=prova3&tr=root&tb=base_db.xml

KPrime OWL transformations:

    http://localhost:7199/owl?prj=prova3&tr=root&tb=base_db.xml


## REFERENCES

https://github.com/OntoUML/ontouml-server

https://github.com/unibz-core/ontouml-models

