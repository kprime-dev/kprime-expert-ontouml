run:
	java -jar target/kprime-expert-ontouml-1.0-SNAPSHOT-jar-with-dependencies.jar

build:
	mvn clean package

build-run: build run

deploy:
	scp ./target/kprime-expert-ontouml-1.0-SNAPSHOT-jar-with-dependencies.jar root@$$DOM:/root/kprime/

docker-build:
	sudo docker build -t kprime-expert-ontouml .

docker-run:
	sudo docker run -it \
		-v /home/nipe/.kprime/expert-ontouml/:/kprime-expert-ontouml/  \
		-e "KPRIME_ONTOUML_HOME=/kprime-expert-ontouml/" \
		-p 7199:7199 \
		--rm kprime-expert-ontouml

# make docker-deploy -e "kpver=beta16"
docker-deploy: docker-build
	sudo docker tag kprime-expert-ontouml nipedot/kprime-expert-ontouml:$(kpver)
	sudo docker push nipedot/kprime-expert-ontouml:$(kpver)
